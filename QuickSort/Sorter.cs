﻿using System;

// ReSharper disable InconsistentNaming
namespace QuickSort
{
    public static class Sorter
    {
        /// <summary>
        /// Sorts an <paramref name="array"/> with quick sort algorithm.
        /// </summary>
        public static void QuickSort(this int[]? array)
        {
            // TODO #1. Implement the method using a loop statements.
            throw new NotImplementedException();
        }

        /// <summary>
        /// Sorts an <paramref name="array"/> with recursive quick sort algorithm.
        /// </summary>
        public static void RecursiveQuickSort(this int[]? array)
        {
            // TODO #2. Implement the method using recursion algorithm.
            throw new NotImplementedException();
        }
    }
}
